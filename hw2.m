generate_gaussian_plots(1, 0, 0, 1, 1, 0.75)
generate_gaussian_plots(2, 1, 2, 2, 1, -0.75)
generate_gaussian_plots(3, -2, 0, 2, 2, 0)



function generate_gaussian_plots(i_plot, x_mean, y_mean, x_std, y_std, rho)
    n_plots = 3;

    %set plotting parameters
    x_max = 8;
    y_max = 8;
    N = 100;
    
    %compute meshgrid of X,Y coordinates
    x_vec  = linspace(-x_max, x_max, N);
    y_vec  = linspace(-y_max, y_max, N);
    [X,Y] = meshgrid(x_vec, y_vec);
    
    
    f = (1 / (2 * pi * x_std * y_std * sqrt(1 - rho^2))) * ...
           exp((-1 / (2 * (1 - rho^2))) * (((X - x_mean) / x_std).^2 - ...
	        2 * rho * (X - x_mean) .* (Y - y_mean) / (x_std * y_std) + ((Y-y_mean)/y_std).^2));

    %make image of the density function
    ax = subplot(3, 3, n_plots * (i_plot - 1) + 1);
    imagesc(x_vec,y_vec,f);
    set(ax,'fontsize',14,'fontweight','bold','ydir','normal');
    xlabel('x');
    ylabel('y');
    
    %make mesh surface of the density function
    ax = subplot(3, 3, n_plots * (i_plot - 1) + 2);
    surf(x_vec,y_vec,f);
    set(gca,'fontsize',14,'fontweight','bold');
    xlabel('x');
    ylabel('y');
    
    %make contour plot of the density function
    ax = subplot(3, 3, n_plots * (i_plot - 1) + 3);
    contour(x_vec,y_vec,f);
    set(gca,'fontsize',14,'fontweight','bold');
    xlabel('x');
    ylabel('y');
end



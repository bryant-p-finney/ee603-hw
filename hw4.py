'''
Created on Oct 6, 2018

Author: bryant.finney@uah.edu
'''
import logging
from matplotlib import pyplot as p
import numpy as np
import pandas
import sys

logger = logging.getLogger(__name__)


class TripleMarkov(object):

    def __init__(self, trans_probs: pandas.DataFrame):
        logger.info("Given:\n{}\n\n".format(str(trans_probs)))
        self.trans_probs = trans_probs

        # calculate the probability matrix for each model at n = 0
        self.pmats = pandas.Series(dtype=np.float64)
        for imodel, model in trans_probs.items():
            self.pmats[imodel] = np.matrix([[model["P00"], 1 - model["P00"]],
                                            [1 - model["P11"], model["P11"]]])

            logger.debug("prob_mats['{}']:\n{}\n".format(imodel,
                                                         self.pmats[imodel]))

    def propagate(self, nsteps):
        # create a container for storing the probability of being in state 1
        # at time n
        p = pandas.DataFrame(index=sorted(self.pmats.keys()), dtype=np.float64)
        p[0] = 1

        # loop across the specified range, and calculate p[n+1] for each model
        for n in range(nsteps):
            for imodel, P in zip(p[n].keys(), self.pmats):
                p[n + 1][imodel] = (p[n][imodel] * P[1, 1] +
                                    (1 - p[n][imodel]) * P[0, 1])

        return p


def main():
    logging.basicConfig(format="%(levelname)s   %(msg)s",
                        level=logging.DEBUG, stream=sys.stdout)

    trans_probs = pandas.DataFrame(index=["P00", "P11"],
                                   dtype=np.float64)

    trans_probs["model_0"] = [0.2, 0.8]
    trans_probs["model_1"] = [0.2, 0.5]
    trans_probs["model_2"] = [0.2, 0.2]

    triple_markov = TripleMarkov(trans_probs)
    p = triple_markov.propagate(50)
    p.plot()


if __name__ == "__main__":
    main()

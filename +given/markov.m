function[mc1, mc2, mc3] = markov(b, p1, p2, p3, N)

    mc1 = zeros(1, N);
    mc2=0*ones(1,N);
    mc3=0*ones(1,N);

    for i=1:N
        mc1(i) = 0.25 * (b ^ 2) * ((2 * p1 - 1) ^ (i - 1));
        mc2(i) = 0.25 * (b ^ 2) * ((2 * p2 - 1) ^ (i - 1));
        mc3(i) = 0.25 * (b ^ 2) * ((2 * p3 - 1) ^ (i - 1));
    end
 
    x = linspace(0,N-1,N);
    plot(x,mc1,x,mc2,x,mc3)
    title('covariance of Markov Sequences')
    xlabel('Lag interval')
    ylabe1('covariance value')
close all;
clear all;
clc;

%set distribution parameters
muX    = 0;
muY    = 0;
sigmaX = 2;
sigmaY = 1;
rho    = 0.8;

%set plotting parameters
xMax   = 8;
yMax   = 8;
N      = 100;

%compute meshgrid of X,Y coordinates
xVec  = linspace(-xMax,xMax,N);
yVec  = linspace(-yMax,yMax,N);
[X,Y] = meshgrid(xVec,yVec);

%compute the 2D function
f = (1/(2*pi*sigmaX*sigmaY*sqrt(1-rho^2)))*exp(   (-1/(2*(1-rho^2)))*(   ((X-muX)/sigmaX).^2 - ...
    2*rho*(X-muX).*(Y-muY)/(sigmaX*sigmaY) + ((Y-muY)/sigmaY).^2     )        );

%make image of the density function
figure;
imagesc(xVec,yVec,f);
set(gca,'fontsize',14,'fontweight','bold','ydir','normal');
xlabel('x');
ylabel('y');

%make mesh surface of the density function
figure;
surf(xVec,yVec,f);
set(gca,'fontsize',14,'fontweight','bold');
xlabel('x');
ylabel('y');

%make contour plot of the density function
figure;
contour(xVec,yVec,f);
set(gca,'fontsize',14,'fontweight','bold');
xlabel('x');
ylabel('y');


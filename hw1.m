close('all'); clear all;

%% Given
mu = 2;
variance = 6;

%% Problem 1
gain = sqrt(72);                               % see Matlab Problems.pdf for derivation of theoretical values
offset = 0.5 * (4 - sqrt(72));

x = gain * rand(1024,1) + offset;


%% Problem 2
gain = sqrt(6);                                % because the distribution is normal, the gain and offset are the standard
offset = 2;                                    % deviation and mean, respectively

y = gain * randn(1024, 1) + offset;


%% Problem 3

% create vectors for the theoretical mean and std. dev. bars
mean_ = 2 * ones(1024, 1);
std_ = [mean_ + sqrt(6), mean_ - sqrt(6)];

% calculate the mean and std. dev. for each distribution
xmean = mean(x) * ones(1024, 1);
xstd = [xmean + std(x), xmean - std(x)];

ymean = mean(y) * ones(1024, 1);
ystd = [ymean + std(y), ymean - std(y)];

% create the figure
f = figure();
ax = axes(f);
title(ax, '(Problem 3) Values of $x \sim U[a, b]$ and $y \sim N(\sigma, \mu^2)$', 'interpreter', 'latex');
hold(ax, 'on');
xlabel(ax, '$i = 1..n$', 'interpreter', 'latex');
ylabel(ax, '$x,y$', 'interpreter', 'latex');

% plot the line series with a display name
plot(ax, mean_, 'k', 'linestyle', '-');
plot(ax, std_(:, 1), 'k', 'linestyle', '--');

plot(ax, x, 'b.');     % the first set of random variables
plot(ax, xmean, 'b', 'linestyle', '-');
plot(ax, xstd(:, 1), 'b', 'linestyle', '--');

plot(ax, y, 'r.');     % the second set of random variables
plot(ax, ymean, 'r', 'linestyle', '-');
plot(ax, ystd(:, 1), 'r', 'linestyle', '--');

labels = {'Theoretical $\mu$', 'Theoretical $\mu \pm \sigma$', ...             % theory
          '$x$', '$\mu_x$', '$\mu_x \pm \sigma$', ...       % uniform
	  '$y$', '$\mu_y$', '$\mu_y \pm \sigma$'};          % gaussian

legend(labels, 'autoupdate', 'off', 'interpreter', 'latex');

% plot the line series without a display name
plot(ax, std_(:, 2), 'k', 'linestyle', '--');
plot(ax, xstd(:, 2), 'b', 'linestyle', '--');
plot(ax, ystd(:, 2), 'r', 'linestyle', '--');

%% Problem 4
f_hist = figure();

% plot the probability densities
ax = subplot(211);
hold(ax, 'on');
title(ax, '(Problem 4) Probability densities for $x \sim U[a, b]$ and $y \sim N[\mu, \sigma]$', ...
      'interpreter', 'latex');
xlim(ax, [min(y), max(y)]);
xlabel(ax, '$x$', 'interpreter', 'latex');
ylabel(ax, '$pdf(x)$', 'interpreter', 'latex');

[counts, bins] = hist(ax, x);
yvals = counts / (length(x) * (bins(2) - bins(1)));

bar(ax, bins, yvals);

a = (4 - sqrt(72)) / 2;            % see Matlab Problems.pdf for derivation of theoretical values
b = (4 + sqrt(72)) / 2;

xvals = [min(x), a, a, b, b, max([x])];
yvals = [0, 0, 1 / (b - a), 1 / (b - a), 0, 0];

plot(ax, xvals, yvals, 'r-');

legend({'Calculated $x$', 'Theoretical $x$'}, 'interpreter', 'latex');

ax = subplot(212);
hold(ax, 'on');
xlabel(ax, '$y$', 'interpreter', 'latex');
ylabel(ax, '$pdf(y)$', 'interpreter', 'latex');

[counts, bins] = hist(y);
bar(ax, bins, counts / (sum(counts) * (bins(2) - bins(1))));

xvals = linspace(min(y), max(y), 1000);
yvals = normpdf(xvals, 2, sqrt(variance));

plot(ax, xvals, yvals, 'r-');

legend(ax, {'Calculated $y$', 'Theoretical $y$'}, 'interpreter', 'latex');

%% Problem 5
f_sq = figure();
% plot the probability densities
ax = subplot(211);
title(ax, '(Problem 5) Probability Densities', 'interpreter', 'latex')
hold(ax, 'on');
xlabel(ax, '$x^2$', 'interpreter', 'latex');
ylabel(ax, '$pdf(x)^2$', 'interpreter', 'latex');

[counts, bins] = hist(ax, x .^ 2, 30);
bar(ax, bins, counts / (length(x) * (bins(2) - bins(1))));

xvals = linspace(0, max(x .^ 2), 1000);
yvals = (b - a) ^ -1 ./ (2 * sqrt(xvals)) .* (0 < xvals & xvals < a .^ 2) + ...
      (b - a) ^ -1 ./ (2 * sqrt(xvals)) .* (0 < xvals & xvals < b .^ 2); 

plot(ax, xvals, yvals, 'r-');

ax = subplot(212);
hold(ax, 'on');
xlabel(ax, '$y^2$', 'interpreter', 'latex');
ylabel(ax, '$pdf(y)^2$', 'interpreter', 'latex');

[counts, bins] = hist(ax, y .^ 2, 30);
bar(ax, bins, counts / (length(y) * (bins(2) - bins(1))));

xvals = linspace(0, max(y .^ 2), 1000);
yvals = 2 ./ (2 * sqrt(xvals)) .* normpdf(sqrt(xvals), 2, 6);

plot(ax, xvals, yvals, 'r-');

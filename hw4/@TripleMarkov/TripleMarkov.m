classdef TripleMarkov
    properties
        % initial state of each mrkov chain
        b = 1;
        
        % probability row vectors for each of the three markov chains:
        p1
        p2
        p3
    end % properties

    methods
        % TRIPLEMARKOV -   Create a markov chain for each of the given 
        %                     
        function obj = TripleMarkov(p1, p2, p3)
            obj.p1 = p1;
            obj.p2 = p2;
            obj.p3 = p3;
            
            % todo: calculate P
        end % function
        
        function [mc1, mc2, mc3] = propagate(obj, nsteps)
            mc1 = zeros(1, nsteps);
            mc2 = zeros(1, nsteps);
            mc3 = zeros(1, nsteps);

            for n = 1:nsteps
                mc1(n) = 0.25 * (obj.b ^ 2) * (2 * obj.p1 - 1) ^ (n - 1);
                mc2(n) = 0.25 * (obj.b ^ 2) * (2 * obj.p2 - 1) ^ (n - 1);
                mc3(n) = 0.25 * (obj.b ^ 2) * (2 * obj.p3 - 1) ^ (n - 1);
            end
        end
    end % methods
 end % classdef  